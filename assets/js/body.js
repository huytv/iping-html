

var lastScrollTop = 0;
$(window).scroll(function(event){
    if($('.box-contact').length) {
        var st = $(this).scrollTop();
        const fullHeight = $(document).height();
        let currentHeight = $(window).scrollTop() + $(window).height();
        let heightTop = $(window).scrollTop();
        if(!$('.box-contact').hasClass('not-show')) {
            if (st > lastScrollTop){
                // if(heightTop > 100) {
                //     $('.box-contact').removeClass('hidden');
                // }
                if(currentHeight > fullHeight - 200) {
                    $('.box-contact').removeClass('hidden');
                }
            } else {
                if((fullHeight - currentHeight) > 350) {
                    $('.box-contact').addClass('hidden');
                }
            }
            lastScrollTop = st;
        }
    }
});

$(function () {
    let is_mobile = false;
    if($('.jump-to').length && $('.jump-to').css('display') == 'block') is_mobile = true;
    $(document).on('click', '#close-contact', function() {
        $('.box-contact').addClass('hidden not-show');
    }).on('click', '.scrollToDiv', function(e) {
        e.preventDefault();
		var target = $(this).attr("href") || $(this).attr('data-id');
        if(is_mobile) {
            if($('.box-menu.box-menu__mobile').hasClass('fixed')) {
                $(window).scrollTop($(target).offset().top - 150);
            } else {
                if($('.full-width-1024').length) {
                    let width1024 = $('.full-width-1024').css('width');
                    if(width1024 == '960px') {
                        $(window).scrollTop($(target).offset().top - 200);
                    } else {
                        $(window).scrollTop($(target).offset().top - 800);
                    }
                } else if($('.all-products .box-right').length) {
                    let width = $('.all-products .box-right').css('width');
                    if(width <= '480px') {
                        $(window).scrollTop($(target).offset().top - 800);
                    } else if(width > '480px' || width <= '1024px') {
                        $(window).scrollTop($(target).offset().top - 600);
                    }
                }
                
            }
        }
        else $(window).scrollTop($(target).offset().top - 100);
        if(is_mobile) {
            $('.box-menu.box-menu__mobile').toggleClass('open');
            $('.jump-to').toggleClass('show');
        }
        return false;
    }).on('click', '.jump-to', function() {
        if(is_mobile) {
            $('.box-menu.box-menu__mobile').toggleClass('open');
            $(this).toggleClass('show');
        }
        
    })
    if ($('.box-menu').length > 0) {
        var top = $('.box-menu').position().top - parseFloat($('.box-menu').css('marginTop').replace(/auto/, 0));
        var footTop = $('.new-footer').position().top - parseFloat($('.new-footer').css('marginTop').replace(/auto/, 0));
        var maxY = footTop - $('.box-menu').outerHeight();
        $(window).scroll(function(evt) {
            if($('.box-menu').length) {
                var y = $(this).scrollTop();
                if (y > top) {
                    if (y < (maxY - 100)) {
                        $('.box-menu').removeClass('fixed-bt').addClass('fixed').removeAttr('style');
                    } else {
                        if(!is_mobile) {
                            if($('.box-menu').hasClass('box-menu-pr')) {
                                var elementTarget = document.getElementById("7");
                                if (y > (elementTarget.offsetTop - 400)) {
                                    $('.box-menu').addClass('fixed-bt');
                                } else {
                                    $('.box-menu').removeClass('fixed-bt');
                                }
                            } else {
                                $('.box-menu').removeClass('fixed').css({
                                    position: 'absolute',
                                    bottom: 0
                                });
                            }
                            
                        }
                    }
                } else {
                    $('.box-menu').removeClass('fixed');
                }
            }
        });
    }

    //owl carousel page giải pháp cds - chi tiết sản phẩm
    if($('.detail-product__carousel').length) {
        $('.detail-product__carousel').owlCarousel({
            margin:20,
            nav:true,
            dots: false,
            navText: ['<img src="../../assets/images/carousel-nav.png">', '<img src="../../assets/images/carousel-nav.png">' ],
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    stagePadding: 100,
                    items:2
                }
            }
        })
    }
});

$(window).scroll(function() {
    if($('.box-menu__solition').length) {
        var scrollDistance = $(window).scrollTop();
        $('.list-solution').each(function(i) {
            if (($(this).position().top - 200 ) <= scrollDistance) {
                $('.box-menu__solition li.active').removeClass('active');
                $('.box-menu__solition li').eq(i).addClass('active');
            }
        });
        $('.detail').each(function(i) {
            if (($(this).position().top - 200 ) <= scrollDistance) {
                $('.box-menu__title.box-menu__active').removeClass('box-menu__active');
                $('.box-menu__title').eq(i).addClass('box-menu__active');
            }
        });
        $('.detail-child').each(function(i) {
            if (($(this).position().top - 200 ) <= scrollDistance) {
                $('.box-menu__solition li.active').removeClass('active');
                $('.box-menu__solition li').eq(i).addClass('active');
            }
            let parent = $('.box-menu__solition').closest('.box-menu__item');
            if(!parent.find('.box-menu__title').hasClass('box-menu__active')) {
                $('.box-menu__solition li.active').removeClass('active');
            }
        });
    }
}).scroll();

jQuery(document).ready(function(){
    $(".js-faq-item").click(function(){
        $(this).children('.icon-toggle').stop().toggleClass('is-close');
        $(this).next().stop().fadeToggle();
    });
    // $(".js-faq-item").first().click();

    let handl_search_nav = () => {
        $('.js-search').find('img').on('click',function() {
            console.log('ok')
            $(this).parent().find('input').toggleClass('show-search');
        })
    }
    handl_search_nav();
});

jQuery(document).ready(function($){
    $('.js-menu-hamburger').click(function(){
        $(this).stop().toggleClass('is-active');
        $('.js-header-popup-close').addClass('d-block');
        if ($(window).width() > 1024) {
            $('.js-header-popup-search').stop().toggleClass('is-active');
        } else {
            $('.js-header-popup-search').addClass('hidden-in-menu');
        }
        $('.js-header-popup-menu').stop().toggleClass('is-active');
        $('.js-header').stop().toggleClass('is-below');
    });
    
    $('.js-search-trigger').click(function(event){
        event.preventDefault();
        $('.js-menu-hamburger').addClass('is-active');
        $('.js-header-popup-close').addClass('d-block');
        $('.js-header-popup-menu, .js-header-popup-search').addClass('is-active');
        $('.js-header').addClass('is-below');
        $('.js-search-focus').focus();
    });

    $('.js-header-popup-close').click(function(){
        $(this).removeClass('d-block');
        $('.js-menu-hamburger, .js-header-popup-menu, .js-header-popup-search').removeClass('is-active');
        $('.js-header').removeClass('is-below');
        if ($(window).width() <= 480) {
            $('.js-header-popup-search').removeClass('hidden-in-menu');
        }
    });

    $('.js-header-popup-menu > ul > li.has-children > a').click(function(){
        $('.js-header-popup-menu > ul > li').removeClass('is-active');
        $(this).parent().addClass('is-active');
    });

    $('.js-prev-menu').click(function(event){
        event.preventDefault();
        $(this).parents('.has-children').removeClass('is-active');
    });

    if ($(window).width() < 768) {
        $('.js-header-popup-menu > ul > li.has-children').removeClass('is-active');
    }
});

/**
 * Chat bot AI CMC 
 */
jQuery(document).ready(function($){
    $('.listening-btn').click();
    // $('.chatbox-body img[alt="micro"]').click();
    if($(".js-bot-search").length) {
        let form_bot_top =  $(".js-bot-search").offset().top;
        let form_bot_left = $(".js-bot-search").offset().left;
        let form_bot_width = $(".js-bot-search").width() + 10;
        let form_bot_height = $(".js-bot-search").height();

        $('#root .chat-popup').addClass('position-absolute');
        $('#root .chat-popup').css({
            "top": form_bot_top + form_bot_height,
            "left": form_bot_left,
            "width": form_bot_width + "px",
            "z-index": 100000,
            "border": "none",
            "max-width": "none",
        });

        $('#root .chatbox-header').hide();
        $('#root .chatbox-body img[alt="micro"]').attr("src", "../assets/images/mic.svg");
        $('#root .chatbox-body img[alt="micro"]').attr("style", "position: relative; z-index: 10");
        $('#root .chatbox-body .chatbox-input').next().attr("style", "position: relative; overflow: hidden");
        // $('#root .chatbox-body .chatbox-input').next().append(`<svg class="mic-loading" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: rgb(0, 65, 159); display: block;" width="200px" height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
        // <circle cx="50" cy="50" r="0" fill="none" stroke="#ffffff" stroke-width="2">
        //   <animate attributeName="r" repeatCount="indefinite" dur="1.5384615384615383s" values="0;40" keyTimes="0;1" keySplines="0 0.2 0.8 1" calcMode="spline" begin="0s"></animate>
        //   <animate attributeName="opacity" repeatCount="indefinite" dur="1.5384615384615383s" values="1;0" keyTimes="0;1" keySplines="0.2 0 0.8 1" calcMode="spline" begin="0s"></animate>
        // </circle><circle cx="50" cy="50" r="0" fill="none" stroke="#ffffff" stroke-width="2">
        //   <animate attributeName="r" repeatCount="indefinite" dur="1.5384615384615383s" values="0;40" keyTimes="0;1" keySplines="0 0.2 0.8 1" calcMode="spline" begin="-0.7692307692307692s"></animate>
        //   <animate attributeName="opacity" repeatCount="indefinite" dur="1.5384615384615383s" values="1;0" keyTimes="0;1" keySplines="0.2 0 0.8 1" calcMode="spline" begin="-0.7692307692307692s"></animate>
        // </circle>
        // </svg>`);
        $('#root .chatbox-body .chatbox-input').next().append(`<svg class="mic-loading" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: rgb(0, 65, 159); display: block;" width="200px" height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
        <circle cx="50" cy="50" r="0" fill="none" stroke="#ffffff" stroke-width="2">
          <animate attributeName="r" repeatCount="indefinite" dur="2.6315789473684212s" values="0;40" keyTimes="0;1" keySplines="0 0.2 0.8 1" calcMode="spline" begin="0s"></animate>
          <animate attributeName="opacity" repeatCount="indefinite" dur="2.6315789473684212s" values="1;0" keyTimes="0;1" keySplines="0.2 0 0.8 1" calcMode="spline" begin="0s"></animate>
        </circle><circle cx="50" cy="50" r="0" fill="none" stroke="#ffffff" stroke-width="2">
          <animate attributeName="r" repeatCount="indefinite" dur="2.6315789473684212s" values="0;40" keyTimes="0;1" keySplines="0 0.2 0.8 1" calcMode="spline" begin="-1.3157894736842106s"></animate>
          <animate attributeName="opacity" repeatCount="indefinite" dur="2.6315789473684212s" values="1;0" keyTimes="0;1" keySplines="0.2 0 0.8 1" calcMode="spline" begin="-1.3157894736842106s"></animate>
        </circle>
        </svg>`);

        $(".js-bot-search").click(function() {
            location.reload();
        });

        $("#root .chatbox-body form > div").click(function() {
            $(this).addClass('is-visible');
        });
    
        var e = $.Event( "keyup", { which: 13 } );
        $(".js-form-bot-search").submit(function(event){
            console.log($(".js-input-fake").val());
            // event.preventDefault();
            $('#root .chatbox-input').val($(".js-input-fake").val());
            $('#root .chatbox-input').attr('value', $(".js-input-fake").val());
            // $('#root .chatbox-input').trigger(e);
            $('#root .chatbox-input').trigger("change");
            return true;
        });

        $('#root .chatbox-body > div').hide();

        $('#root .chatbox-input').focus(function(){
            $(".js-banner-data-content").addClass('d-none');
            $('#root .chatbox-body > div').fadeIn();
            $('.js-onclick').attr('style', "margin-top: 248px !important; margin-left: 9px !important");
            $('#root .chatbox-body').addClass('is-active');
        });

        $('#root .chatbox-input').next().click(function(){
            let _this = $(this);
            $(".js-banner-data-content").addClass('d-none');
            $('#root .chatbox-body > div').fadeIn();
            $('.js-onclick').attr('style', "margin-top: 248px !important; margin-left: 9px !important");
            $('#root .chatbox-body').addClass('is-active');
            
            _this.addClass('is-loading');
            setTimeout(function(){
                _this.removeClass('is-loading');
            }, 5000);
        });

        setInterval(function(){
            $('#root .chatbox-input').prop("disabled", false);
        }, 1000);
    }
});

function reset_chat_bot() {
    $('#root .chatbox-body').find('small').empty();
    $('#root .chatbox-body > div').hide();
    $('.js-onclick').attr('style', "margin-top: 0; margin-left: 0");
    $('#root .chatbox-body').removeClass('is-active');
}

jQuery(document).ready(function($){
    $('.js-banner-head').each(function(){
        let _this = this;
        $(this).click(function(){
            let anchor = $(this).attr('data-head');

            reset_chat_bot();
            $('.js-banner-head').not(_this).removeClass("is-active");
            $(this).addClass("is-active");

            $('.js-banner-data-content').not(anchor).addClass('d-none');
            $(anchor).removeClass('d-none');
        });
    }); 

    if ($('.js-banner-head-fisrt').length) {
        $('.js-banner-data-content').addClass('d-none');
        $('.js-banner-head').removeClass("is-active");
        
        $('.js-banner-head-fisrt').addClass('is-active');
        $('#anchor-data-1').removeClass('d-none');
    }
});

jQuery(document).ready(function($){
    $('.js-box-mg-head').each(function(){
        let _this = this;
        $(this).click(function(){
            let anchor = $(this).attr('data-head');

            reset_chat_bot();
            $('.js-box-mg-head').not(_this).removeClass("is-active");
            $(this).addClass("is-active");

            $('.js-box-mg-data-content').not(anchor).addClass('is-hidden');
            $(anchor).removeClass('is-hidden');
        });
    }); 

    if ($('.js-box-mg-head-fisrt').length) {
        $('.js-box-mg-data-content').addClass('is-hidden');
        $('.js-box-mg-head').removeClass("is-active");
        
        $('.js-box-mg-head-fisrt').addClass('is-active');
        $('#anchor-data-1').removeClass('is-hidden');
    }
});


// xử lý countdown
$(document).ready(function () {
    let handl_countDown = () => {

    // Get value from attr HTML
    var get_date_attr = $( '.timer-view' ).attr( "data-date" );

    var countDownDate = new Date(get_date_attr).getTime();
    var x = setInterval(function() {

    var now = new Date().getTime();
    
    var distance = countDownDate - now;
    
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    if ((days.toString().length) = 1) { 
            days = '0' + days;
    }
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
    if (document.getElementById('clockdiv')) {
        document.getElementById('clockdiv').innerHTML = `
                                            <div id="clockdiv" class="timer-view">
                                            <div class="block-timer">
                                                <p class="days --bg">${days}</p>
                                                <div class="smalltext">NGÀY</div>
                                            </div>
                                            <div class="block-timer">
                                                <p class="hours --bg">${('0' + hours).slice(-2)}</p>
                                                <div class="smalltext">GIỜ</div>
                                            </div>
                                            <div class="block-timer">
                                                <p class="minutes --bg">${('0' + minutes).slice(-2)}</p>
                                                <div class="smalltext">PHÚT</div>
                                            </div>
                                            <div class="block-timer">
                                                <p class="seconds --bg">${('0' + seconds).slice(-2)}</p>
                                                <div class="smalltext">GIÂY</div>
                                            </div>
                                            </div>
                                        `;
    }
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("clockdiv").innerHTML;
        }
    }, 1000);
    }
    handl_countDown();    
});

$(document).ready(function () {
    $('.owl-story').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })
});

jQuery(document).ready(function($) {
    if ($(window).width() > 1024) {
        $('.js-nice-select').niceSelect();
    }
});

jQuery(document).ready(function($){
    if ($(window).width() <= 1024) {
        $('.js-btn-open-search').click(function(){
            $('.js-header-popup-search').addClass('is-mobile-show');
        });
        $('.js-btn-close-search').click(function(){
            $('.js-header-popup-search').removeClass('is-mobile-show');
        });
    }
});

jQuery(document).ready(function ($) {
    $('.js-open-popup-register').click(function (event) {
        event.preventDefault();
        $('body, html').addClass('overflow-hidden');
        $('.js-popup-register, .js-close-popup-register').addClass('is-active');
    });
    $('.js-close-popup-register').click(function () {
        event.preventDefault();
        $('body, html').removeClass('overflow-hidden');
        $('.js-popup-register, .js-close-popup-register').removeClass('is-active');
    });
});

jQuery(document).ready(function ($) {
    $('.js-open-popup-login').click(function (event) {
        event.preventDefault();
        $('body, html').addClass('overflow-hidden');
        $('.js-popup-login, .js-close-popup-login').addClass('is-active');
    });
    $('.js-close-popup-login').click(function (e) {
        e.preventDefault();
        $('body, html').removeClass('overflow-hidden');
        $('.js-popup-login, .js-close-popup-login').removeClass('is-active');
    });
});

jQuery(document).ready(function($){
    let w = $(window).width();
    $(window).resize(function(){
        if (w - $(window).width() > 10 || w - $(window).width() < -10) {
            location.reload();
        }
    });

    $('.input-search-header').on('keyup', function() {
        var value = $.trim(removeVietnameseFromString($(this).val()));
        $("ul.option-search li:not(.not)").filter(function(i, v) {
            var text_search = removeVietnameseFromString($(this).find('a').text());
            $(this).toggle(text_search.indexOf(value) > -1)
        });
    })

    $(document).on('click', '.icon-option.icon-remove', function() {
        let parent = $(this).closest('li');
        parent.addClass('not').hide();
    });

    $("body").click(function(event) {
        if (event.target.id == 'input-search-header') {
            $('.option-search').addClass('show-result');
            $('.overlay').show();
        } else {
            let container1 = $("#input-search-header");
            let container2 = $(".option-search");
            // if the target of the click isn't the container nor a descendant of the container
            if (!container1.is(event.target) && container1.has(event.target).length === 0 && !container2.is(event.target) && container2.has(event.target).length === 0) 
            {
                $('.option-search').removeClass('show-result');
                $('.overlay').hide();
            }
        }
    });
});

function removeVietnameseFromString(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    str = str.toLowerCase();
    return str;
}
